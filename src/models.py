from peewee import *

#
# This file was generated with peewee's pwiz:
# pwiz.py -e mysql -u laravel -P laravel laravel > src/models.py
#

CHAR_MAX_LENGTH = 400

database = MySQLDatabase('laravel', **{'password': 'laravel', 'user': 'laravel'})

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class Persons(BaseModel):
    comment = CharField(null=True)
    company_name = CharField(null=True)
    created_at = DateTimeField(null=True)
    deleted_at = DateTimeField(null=True)
    email = CharField()
    firstname = CharField(null=True)
    name = CharField()
    number = CharField()
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'persons'

class Addresses(BaseModel):
    person = ForeignKeyField(db_column='person_id', rel_model=Persons, to_field='id')
    code = CharField()
    country = CharField(null=True)
    created_at = DateTimeField(null=True)
    deleted_at = DateTimeField(null=True)
    line1 = CharField()
    line2 = CharField(null=True)
    line3 = CharField(null=True)
    phone = CharField()
    roudis = IntegerField(null=True)
    town = CharField()
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'addresses'

class MailingLists(BaseModel):
    created_at = DateTimeField(null=True)
    name = CharField()
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'mailing_lists'

class MailingListSubscriptions(BaseModel):
    created_at = DateTimeField(null=True)
    mailing = ForeignKeyField(db_column='mailing_id', rel_model=MailingLists, to_field='id')
    person = ForeignKeyField(db_column='person_id', rel_model=Persons, to_field='id')
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'mailing_list_subscriptions'

class Memberships(BaseModel):
    person = ForeignKeyField(db_column='person_id', rel_model=Persons, to_field='id')
    created_at = DateTimeField(null=True)
    expires_on = DateField()
    joined_on = DateField()
    last_action = CharField(null=True)
    needed_action = CharField(null=True)
    renewed_on = DateField(null=True)
    status = CharField(null=True)
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'memberships'

class Migrations(BaseModel):
    batch = IntegerField()
    migration = CharField()

    class Meta:
        db_table = 'migrations'

class PasswordResets(BaseModel):
    created_at = DateTimeField(null=True)
    email = CharField(index=True)
    token = CharField()

    class Meta:
        db_table = 'password_resets'
        primary_key = False

class Payments(BaseModel):
    person = ForeignKeyField(db_column='person_id', rel_model=Persons, to_field='id')
    # TODO: allow null and run a migration
    amount = FloatField()
    check_number = IntegerField(null=True)  # = don_precisions
    created_at = DateTimeField(null=True)
    paid_on = DateField()
    payment_mode = CharField(max_length=CHAR_MAX_LENGTH, null=True)
    payment_type = CharField(max_length=CHAR_MAX_LENGTH, null=True)
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'payments'

class Users(BaseModel):
    created_at = DateTimeField(null=True)
    email = CharField(unique=True)
    name = CharField()
    password = CharField()
    remember_token = CharField(null=True)
    updated_at = DateTimeField(null=True)

    class Meta:
        db_table = 'users'
