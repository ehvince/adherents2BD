#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

try:
    import pypandoc
    long_description = pypandoc.convert('README.md', 'rst')
except (IOError, OSError, ImportError):
    long_description = read('README.md')

setup(
    name = "adherents2BD",
    version = "0.0.1",
    packages = find_packages(exclude=["contrib", "doc", "tests"]),

    install_requires = [
        "toolz",     # functional utils
        "tabulate",
        "addict",    # fancy dict access
        "termcolor", # colored print
        "unidecode", # string clean up
        "clize==3",  # quick and easy cli args
        "tqdm",      # progress bar
        "termcolor", # terminal color
        "peewee",    # light ORM
        "pymysql",
    ],

    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        # '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        # 'hello': ['*.msg'],
    },

    # metadata for upload to PyPI
    author = "vdardel",
    author_email = "ehvince@mailz.org",
    description = "Acrimed: Adhérents.ods to BD.",
    long_description = long_description,
    license = "GNU LGPLv3",
    url = "https://gitlab.com/vindarel/",

    entry_points = {
        "console_scripts": [
            "adherents2BD = src.adherents2BD:run",
        ],
    },

    tests_require = {

    },

    classifiers = [
        "Environment :: Console",
        "Intended Audience :: Bénévoles d'Acrimed",
        "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
        "Topic :: Utilities",
        "Programming Language :: Python :: 3.5",
    ],

)
