#!/usr/bin/env python
# -*- coding: utf-8 -*-

import peewee
from models import Persons

def delete_adele():
    adele = Persons.select().where(Persons.email.contains("legoff"))
    if adele.count():
        query = adele.get()
        return query.delete_instance(recursive=True)
    return 0

if __name__ == '__main__':
    db_name = "laravel"
    db_user = "laravel"
    db_pwd = "laravel"
    db =  peewee.MySQLDatabase(db_name, user=db_user, password=db_pwd)

    print "deleting 'Adele Legoff' from test db..."
    nb = delete_adele()
    print "done ({} row(s) removed).".format(nb)
