
debian:
	apt install python-pip python-dev

install:
	pip install -e .

models:
	pwiz.py -e mysql -u laravel -P laravel laravel > src/models.py

clean:
	# rm 'Adele Legoff' from test db.
	./src/clean.py
