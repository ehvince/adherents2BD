#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

def extract_postal_code(entry):
    """
    From "75000 PARIS", extract the numbers.
    """
    match = re.match('[0-9\s]+', entry)
    if match:
        return match.group().strip()

def extract_town(entry):
    """
    "75000 Paris" -> remove postal code.
    """
    postal_code = extract_postal_code(entry)
    try:
        return entry.replace(postal_code, '').strip()
    except Exception as e:
        # import ipdb; ipdb.set_trace()
        return entry


def extract_float(it, default=0.0):
    reg = '[0-9\s,.]+'
    match = re.search(reg, it)
    if match:
        res = match.group()
        res = res.replace(',', '.')
        res = res.replace(' ', '')
        try:
            return float(res)
        except:
            print "utils.extract_float warning: could not parse 'montant cotisation': {} (parsed {})".format(it, res)

    return default
