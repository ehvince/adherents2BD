#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
# from unidecode import unidecode
import logging
import sys
from datetime import datetime
from os.path import abspath
from os.path import exists
from os.path import expanduser
from os.path import isfile
from os.path import splitext
from pprint import pprint
from subprocess import call

import peewee
from models import Addresses
from models import Memberships
from models import Payments
from models import Persons
from models import Users

from utils import extract_postal_code
from utils import extract_float
from utils import extract_town


ADHESION_DATE_DEFAULT = "01/01/2000"

DONATION_TYPE = "donation"
MEMBERSHIP_TYPE = "membership"

DEFAULT_COUNTRY = "France (default)"


def convert2csv(odsfile):
    """
    Convert an ods file (LibreOffice Calc) to csv.
    """
    ext = splitext(odsfile)[-1]
    if ext == ".csv":
        return odsfile

    cmd = ["soffice", "--headless", "--convert-to", "csv"]
    cmd.append(odsfile)
    try:
        ret = call(cmd)
    except Exception as e:
        print "Erreur: {}. Did you install LibreOffice ?".format(e)
        return None

    if ret == 0:
        return splitext(odsfile)[0] + ".csv"
    else:
        return None

def guess_delimiter(fieldnames_str, delimiter=";"):
    """
    Devrait quand même être le point-virgule.
    """
    if "," in fieldnames_str:
        return ","
    elif ";" in fieldnames_str:
        return ";"
    else:
        print("what is the current delimiter ? That shouldn't happen")
        return delimiter

SHEETS = {
    "adherents": {"fieldnames_row": 2,  # so as to guess the col name, in case they change. Error-prone parsing.
                  "fieldnames": [
                      "destinataire",
                      "raison_sociale",
                      "adresse_remise",
                      "adresse_batiment",
                      "adresse_voie",
                      "adresse_commune",
                      "postal_code",
                      "country",
                      "distribution",
                      "phone",
                      "email",
                      "adhesion_date_paiement",
                      "adhesion_mode_paiement",
                      "adhesion_precisions",
                      "adhesion_montant",
                      "don_date_paiement",
                      "don_mode_paiement",
                      "don_precisions",
                      "don_montant",
                      "existant",
                      "jours_retard",
                      "a_jour",
                      "premiere_adhesion",
                      "derniere_cotis",
                      "syndicat_asso",
                      "profession",
                      "commentaires_laisses",
                      "commentaires_acrimed",
                      "idf",
                      "listes",
                      "adresses",
                      "a_enregistrer_kits_listes",
                  ]
              },
}


def extractCardData(filename, nofieldsrow=False, delimiter=";"):
    """
    Return: an iterator of dicts that map a field and its value.
    """
    with open(filename, "rU") as f:
        lines = f.readlines()

    sheet_settings = SHEETS.get('adherents')
    field_names = sheet_settings["fieldnames"]

    csvreader = csv.DictReader(lines[4:], fieldnames=field_names, delimiter=delimiter)

    # skip headers.
    csvreader.next()
    csvreader.next()

    return csvreader


def is_email_present(email):
    return Persons.select().where(Persons.email == email).count()

def delete_adele():
    adele = Persons.select().where(Persons.email.contains("legoff")).get()
    return adele.delete_instance()

def parse_date(date, default=""):
    if date:
        try:
            return datetime.strptime(date, "%d-%m-%Y")
        except Exception as e:
            print("*** warning: could not parse date {} and will return default (01-01-2000). {}".format(date, e))
            return ADHESION_DATE_DEFAULT

    return default

def parse_int(it):
    if not it:
        return 0
    try:
        return int(it)
    except Exception:
        return 0

def parse_firstname_name(it):
    """
    Simply take the first word as the firstname.
    It will corrected manually if needed.
    """
    if not it:
        return "", ""
    res = it.split(" ")
    if len(res) >= 2:
        return res[0], res[1]
    return res[0], ""

def add2DB(data):
    """
    Add csv rows to the DB. Create Persons, Addresses, Memberships etc.

    DB config is stored in crm/.env.

    http://peewee.readthedocs.io/en/latest/peewee/querying.html#creating-a-new-record
    """
    db_name = "laravel"
    db_user = "laravel"
    db_pwd = "laravel"
    db =  peewee.MySQLDatabase(db_name, user=db_user, password=db_pwd)

    now = datetime.now()

    with db.atomic():
        for row in data:
            if row['email'] and not is_email_present(row['email']):

                print "looking at:"
                pprint(row)

                derniere_cotis = parse_date(row['derniere_cotis'])
                if not derniere_cotis:
                    derniere_cotis = ADHESION_DATE_DEFAULT
                adhesion_date_paiement = parse_date(row['adhesion_date_paiement'])
                if not adhesion_date_paiement:
                    adhesion_date_paiement = ADHESION_DATE_DEFAULT

                existant = row['existant']
                idf = row['idf']
                jours_retard = row['jours_retard']
                premiere_adhesion = row['premiere_adhesion']
                profession= row['profession']
                prenom, nom = parse_firstname_name(row.get('destinataire'))


                # Missing fields in DB ? Let's put them on Person.comment.
                comment = ""
                for it in ['profession', 'syndicat']:
                    if row.get(it):
                        comment += "{}: {},".format(it, row.get(it))

                print "\t adding {}...".format(row['email'])
                newperson = Persons.create(
                    email=row['email'],
                    name=nom,
                    firstname=prenom,
                    number=10, # TODO: what is it ?
                    comment=comment,
                )

                postal_code = extract_postal_code(row['postal_code'])
                town = extract_town(row['postal_code'])
                country = DEFAULT_COUNTRY

                Addresses.create(
                    person=newperson,
                    code=postal_code or "",
                    town=town,
                    country=country,
                    line1=row['adresse_remise'],
                    line2=row['adresse_voie'],
                    # plus de lignes d'adresse ? ligne 3 et 4
                    phone=row['phone'],
                    roudis=parse_int(row.get('distribution')),
                )

                # Mailing liste ?

                Memberships.create(
                    person=newperson,
                    joined_on=adhesion_date_paiement,
                    renewed_on=derniere_cotis,
                    expires_on=datetime.min,  # needs a default value, corrected later.
                )

                PAYMENT_TYPE = ""

                montant = extract_float(row['adhesion_montant'])
                mode_paiement = row.get('adhesion_mode_paiement', "")
                adhesion_precisions = row.get('adhesion_precisions', "")
                adhesion_date_paiement = parse_date(row['adhesion_date_paiement'])
                if adhesion_date_paiement:
                    PAYMENT_TYPE = MEMBERSHIP_TYPE

                # Don ?
                don_date_paiement = parse_date(row['don_date_paiement'])
                # xxx plus simple si check_number (precisions) était un char, pas un int.
                don_precisions = -1
                if don_date_paiement:
                    PAYMENT_TYPE = DONATION_TYPE
                    mode_paiement = row.get('don_mode_paiement', "")
                    montant = extract_float(row.get('don_montant'))
                    don_precisions = parse_int(row.get('don_precisions'))

                if montant == 0.0:
                    mode_paiement = "exemption"

                Payments.create(
                    person=newperson,
                    amount=montant or 0.0,
                    paid_on=adhesion_date_paiement or don_date_paiement,
                    payment_mode=mode_paiement or "",
                    payment_type=PAYMENT_TYPE,
                    check_number=don_precisions,
                )

    print "... done."


def run(odsfile, nofieldsrow=False):
    """
    :param str odsfile: the .ods file
    """
    csvdata = []
    # csvfile = convert2csv(odsfile)
    csvfile = odsfile
    if exists(expanduser(csvfile)):
        csvdata = extractCardData(expanduser(csvfile), nofieldsrow=nofieldsrow)
        if csvdata:
            print "Adding to the db..."
            add2DB(csvdata)

    return csvdata


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: give an ods file as parameter.")
        exit(0)

    odsfile = sys.argv[1]
    odsfile = abspath(odsfile)
    exit(run(odsfile))
