# Acrimed - Adhérents.xls -> Base de Données

## Installation

## Usage

Exporter les listes au format csv.

Choisir les options à l'exportation:

- séparateur de champs: le point-virgule `;`
- entourer les champs texte par des guillemets
- format: utf-8 (devrait être le choix par défaut)

__Attention__ je trouve des caractères saut de ligne (`^M`) dans mes exports csv, je dois les remplacer par un espace avec mon éditeur de texte.

Appeler le script:

    python src/adherents2BD.py adherents.csv

Suppositions sur le fichier:

- la ligne 2 contient les noms des champs
- la 3 est parfois utilisée par les filtres, parfois c'est le début
  des données.
=> settings hardcodés

**¡ Attention !**

- le "destinataire" comporte noms et prénoms (et civilité ?): ça va
  être difficile à parser. Pbs en vue avec des noms et prénoms
  composés… => créer de nouvelles colonnes dans le fichiers ? Ça
  permettrait à Acrimed de faire des stats sur le genre des adhérents,
  de trier par nom de famille,…

- une date par défaut (01-01-2000) est utilisée quand on ne peut pas parser une donnée en tant que date. Le champ en BD est une date, on ne peut retourner "COMPIL 2017" par exemple.


## Dév

Créer un environnement virtuel Python (apt-get install `virtualenvwrapper`):

    mkvirtualenv acrimed

installer les dépendances:

    make install

L'interaction avec la base de données est faite avec le petit ORM
[Peewee](http://peewee.readthedocs.io/en/latest/peewee/quickstart.html).

Mount your local sources into the image:

    docker run -v /home/me/my/script/:/app/script ...

Enlever les données de test de la BD, pour recommencer:

    make clean


## TODOs

- [X] lier à la base de données.
  - [X] pas réussi à faire marcher l'environnement Docker.
  - [X] installer python et cie dans l'image Docker
  - [X] se connecter à la BD (peewee ORM)
  - [X] comment lier le Docker à mes sources externes pour le développement ou bien se connecter à la BD du Docker ? => `-v`
  - [X] generate models from the existing DB
- [ ] add all fields
